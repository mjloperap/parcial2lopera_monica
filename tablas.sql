CREATE TABLE jugador
	(cedula int(10) not null,
	nombre VARCHAR(30) not null,
	posicion VARCHAR(30) not null,
	PRIMARY KEY (cedula))
ENGINE=InnoDB;

CREATE TABLE equipo
	(codigo int(10) not null,
	nombre VARCHAR(30) not null,
	ciudad VARCHAR(30) not null,
	jugador int(10) not null,
	PRIMARY KEY (cedula, jugador),
	CONSTRAINT e_j FOREIGN KEY (jugador) REFERENCES jugador(cedula))
ENGINE=InnoDB;
