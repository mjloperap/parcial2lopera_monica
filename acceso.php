<?php

require('configs/include.php');

class c_acceso extends super_controller {
	
	public function display()
	{
	
		$contenido_main = 'empty.tpl';
		
		if($this->get->cod != 123){
			$this->error = 1;
			$this->msg_warning = "Zona prohibida ALEJATEEE";
			$this->temp_aux = 'message.tpl';
			$this->engine->assign('type_warning', $this->type_warning);
			$this->engine->assign('msg_warning', $this->msg_warning);
		}
		else{
			$contenido_main = 'acceso.tpl';
		}
		
		$this->engine->assign('title',$this->gvar['n_index']);
		
		$this->engine->display('header.tpl');
		$this->engine->display($this->temp_aux);
		$this->engine->display($contenido_main);
		$this->engine->display('footer.tpl');
	}
	
	public function run()
	{
		$this->display();
	}
}

$call = new c_acceso();
$call->run();

?>