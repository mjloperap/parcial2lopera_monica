<?php

require('configs/include.php');

class c_registrarjugador extends super_controller {
    
    public function add()
    {
        $jugador = new jugador($this->post);

        if(is_empty($jugador->get('cedula')) or is_empty($jugador->get('nombre')) or is_empty($jugador->get('posicion'))){
            throw_exception("Error: Eisten campos vacios");
        }
		
        $this->orm->connect();
        $this->orm->insert_data("normal",$jugador);
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Jugador registrado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
	
    }

    public function display()
    {
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registrarjugador.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e) 
		{
			$this->error=1; $this->msg_warning=$e->getMessage();
			$this->engine->assign('type_warning',$this->type_warning);
			$this->engine->assign('msg_warning',$this->msg_warning);
			$this->temp_aux = 'message.tpl';
		}    
        $this->display();
    }
}

$call = new c_registrarjugador();
$call->run();

?>
