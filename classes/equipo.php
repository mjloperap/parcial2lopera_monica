<?php
 
class equipo extends object_standard
{
	//attributes
	protected $codigo;
	protected $nombre;
	protected $ciudad;
	protected $jugador;
		
	//components
	var $components = array();
	
	//auxiliars for primary key and for files
	var $auxiliars = array();
	
	//data about the attributes
	public function metadata()
	{
		return array("codigo" => array(), "nombre" => array(), "ciudad" => array(), "jugador" => array("foreign_name" => "e_j", "foreign" => "jugador", "foreign_attribute" => "cedula")); 
	}

	public function primary_key()
	{
		return array("codigo, jugador");
	}
	
	public function relational_keys($class, $rel_name){
		switch($class){
			case "jugador":
			switch($rel_name){
				case "e_j":
				return array("jugador");
				break;
			}
			break;
			default:
			break;
		}
	}
}

?>