<?php

require('configs/include.php');

class c_listarjugador extends super_controller {
    
     public function listar()
    {
		$equipo = new equipo($this->post);
        if(is_empty($equipo->get('codigo'))){ 
		throw_exception("Debe ingresar un codigo");
		}
		
		$cod['equipo']['codigo']=$this->post->codigo;
		$options['equipo']['lvl2']="by_cod";
		
		$this->orm->connect();
        $this->orm->read_data(array("equipo"),$options,$cod);
		$codcheck = $this->orm->get_objects("equipo");
        $this->orm->close();
		
		if(is_empty($codcheck)){
		throw_exception("Debe ingresar un codigo de equipo existente");
		}
		else{
		$this->engine->assign('boton', "oprimido");
		}
		
    }


    public function display()
    {
		
		$cod['equipo']['codigo']=$this->post->codigo;
		$options['equipo']['lvl2']="by_jug";
		
		$this->orm->connect();
        $this->orm->read_data(array("equipo"),$options,$cod);
		$jugador = $this->orm->get_objects("equipo");
        $this->orm->close();
		
		$this->engine->assign('equipo', $jugador);

		$this->engine->display('header.tpl');
        $this->engine->display('listarjugador.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {

        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e) 
		{
			$this->error=1; $this->msg_warning=$e->getMessage();
			$this->engine->assign('type_warning',$this->type_warning);
			$this->engine->assign('msg_warning',$this->msg_warning);
			$this->temp_aux = 'message.tpl';
		}    
        $this->display();
    }
}

$call = new c_listarjugador();
$call->run();

?>
